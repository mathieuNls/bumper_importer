package com.concordia.bumper.attachment;

import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import com.concordia.bumper.enums.AttachmentType;

/**
 * Implementation of @see AbstractAttachment that downloads only text/plain file
 * that were as attachment on the bug. If the file isn't a text/plain, we only put 
 * the link towards the attachment. Likewise, if something happens while trying to 
 * access to the attachment, we also put the link to it instead.
 * 
 * @author math
 * 
 */
public class FileAttachment extends AbstractAttachment {
	
	/**
	 * Simple super contstructor
	 * @param url
	 * @param type
	 */
	public FileAttachment(String url, AttachmentType type) {
		super(url, type);
	}

	@Override
	public void download() {
		InputStream in = null;
		try {

			//TODO: for performances sack, I deeactivate the download of attachment.
			
//			if (this.url.openConnection().getContentType()
//					.contains("text/plain")) {
//				in = this.url.openStream();
//				this.output = IOUtils.toString(in).replaceAll("&", "&amp;")
//						.replaceAll("<", "&lt;")
//						.replaceAll(">", "&gt;");
//			} else {
//				this.output = this.url.toString();
//			}
			
			
			this.output = this.url.toString().replaceAll("&", "&amp;");

		} catch (Exception e) {
			System.err.println("Problem download file:" + this.url.toString());
			this.output = this.url.toString();
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

}
