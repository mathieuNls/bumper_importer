package com.concordia.bumper.attachment.changeset;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * This class is part of the Adapter pattern @see http://www.oodesign.com/adapter-pattern.html
 * and will be adapted by specialized @see JSoupCommand in order to read changeset in a specialized
 * fashion
 *  
 * @author math
 *
 */
public abstract class AbstractJsoupCommand {
	
	/**
	 * 
	 * @param elem
	 * @return null
	 */
	public Element exec(Element elem){
		System.err.println("Has to be adapated");
		return null;
	}
	
	/**
	 * 
	 * @param doc
	 * @return null
	 */
	public Elements exec(Document doc){
		System.err.println("Has to be adapated");
		return null;
	}
	
	/**
	 * 
	 * @param elem
	 * @return null 
	 */
	public String extract(Element elem){
		System.err.println("Has to be adapated");
		return null;
	}
}
