package com.concordia.bumper.attachment.changeset;

import com.concordia.bumper.enums.AttachmentType;
import com.concordia.bumper.enums.JavaKeyWords;

/**
 * Specialized ChangesetAttachment for Java Programs.
 * 
 * @author math
 *
 */
public class JavaChangesetAttachment extends AbstractChangesetAttachment{
	
	
	/**
	 * Simple super constructeur
	 * 
	 * @param url
	 * @param type
	 * @param identifierToDownload
	 */
	public JavaChangesetAttachment(String url, AttachmentType type,
			String identifierToDownload) {
		super(url, type, identifierToDownload);
	}
	
	public JavaChangesetAttachment(String url, AttachmentType type,
			String identifierToDownload, boolean isPatch) {
		super(url, type, identifierToDownload, isPatch);
	}


	@Override
	public void parse() {
		for(String line : this.output.split("[\\r\\n]+")){
			
			
			
			if(line.matches(".*([0-9])+?.([0-9])+?( \\+).*")){
				
				
				if(checkAgainstKeyWords(line)){
					this.addedStructuralChanges.add(line);
				}
			}else if(line.matches(".*([0-9])+?.([0-9])+?( \\-).*")){
				if(checkAgainstKeyWords(line)){
					this.deletedStructualChanges.add(line);
				}
			}
		}
	}
	
	@Override
	protected boolean checkAgainstKeyWords(String line){
		for(JavaKeyWords jkw : JavaKeyWords.values()){
			if(line.contains(" " + jkw.toString().toLowerCase())){
				return true;
			}
		}
		return false;
	}

}
