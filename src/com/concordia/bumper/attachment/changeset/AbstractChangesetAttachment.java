package com.concordia.bumper.attachment.changeset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.concordia.bumper.attachment.AbstractAttachment;
import com.concordia.bumper.enums.AttachmentType;

/**
 * Defines the behaviour of @see AbstractAttachment that are changeset. 
 * Specialized AbstractChangesetAttachment will download the changeset, 
 * analyszing it to find structural changes based on keyword and coloration.
 * 
 * Also, this abstract class computes some statistics by itself such as 
 * the mutation, the number of files & lines.
 * 
 * To parse the changeset, Jsoup is used @see http://jsoup.org/ and the set of 
 * jsoup of needed jsoup commands is set throught a command pattern @http://www.oodesign.com/command-pattern.html 
 * @see JSoupCommands and an adaptor pattern @see http://www.oodesign.com/adapter-pattern.html 
 * @see AbstractJsoupCommand. 
 * 
 * @author math
 *
 */
public abstract class AbstractChangesetAttachment extends AbstractAttachment {

	protected List<String> addedStructuralChanges = new ArrayList<String>();
	protected List<String> deletedStructualChanges = new ArrayList<String>();
	
	private List<String> files = new ArrayList<String>();
	private Double positiveMutation;
	private Double negativeMutation;
	private int nbLines = 0;
	private String identifierToDownload;

	private List<AbstractJsoupCommand> jsoupCommandsTowardsFile;
	private List<AbstractJsoupCommand> jsoupCommandTowardsFileLinesCount;
	private boolean isPatch = false;

	/**
	 * Simple super constructor
	 * @param url
	 * @param type
	 * @param identifierToDownload
	 */
	public AbstractChangesetAttachment(String url, AttachmentType type,
			String identifierToDownload) {
		super(url, type);
		this.identifierToDownload = identifierToDownload;
	}

	public AbstractChangesetAttachment(String url, AttachmentType type,
			String identifierToDownload, boolean isPatch) {
		super(url, type);
		this.identifierToDownload = identifierToDownload;
		this.isPatch = isPatch;
	}
	
	@Override
	public void download() {
		Document doc = this.readChangeSet();
		String errorHref = "";
		
		if(!isPatch)
		{
			try {
				for(String href : getHrefToFile(doc)){
					this.files.add(href);
					errorHref = href;
					doc = Jsoup.connect(href).get();
					this.nbLines += getFileLineCount(doc);
				}
			} catch (IOException e) {
				System.err.println("Error while retrieving: "+errorHref);
			}
			
		}
		

		this.analyze();
	}

	/**
	 * Compute positive & negative mutation after running the parse
	 */
	private void analyze() {
		this.parse();
		this.positiveMutation = (new Double(this.addedStructuralChanges.size()) / new Double(this.nbLines)) * 100.0D;
		this.negativeMutation = (new Double(this.deletedStructualChanges.size()) / new Double(this.nbLines)) * 100.0D;
	}

	/**
	 * Defines how language specialized ChangsetAttachment shoudld read 
	 * the changset. This method computes the structural changes while
	 * reading the changeset.
	 */
	public abstract void parse();
	
	/**
	 * Connects to the changeset url, 
	 * Reads the changset, removes the html tag and populate the 
	 * this.output value. If something goes wrong, the output value
	 * is populated with the link. 
	 * 
	 * @return Document changeset
	 */
	private Document readChangeSet() {
		Document doc = null;
		try {
			doc = Jsoup.connect(this.url.toString()).timeout(10*1000).get();
			for (Element element : doc
					.getElementsByTag(this.identifierToDownload)) {
				this.output += element.text().replaceAll("&", "&amp;")
						.replaceAll("<", "&lt;")
						.replaceAll(">", "&gt;");;
			}
		} catch (IOException e) {
			this.output = this.url.toString();
			System.err.println("Error Jsouping: " + this.url.toString());
		}
		return doc;
	}
	
	/**
	 * Uses the @see JSoupCommand to count the number of lines of
	 * each file in a changset. This will be used for the mutation 
	 * statistics
	 * 
	 * @param doc
	 * @return number of lines
	 */
	private int getFileLineCount(Document doc) {
		Element elem = null;

		for (int i = 0; i < jsoupCommandTowardsFileLinesCount.size() - 1; i++) {
			if (elem == null) {
				elem = jsoupCommandTowardsFileLinesCount.get(i).exec(doc).get(0);
			} else {
				elem = jsoupCommandTowardsFileLinesCount.get(i).exec(elem);
			}
		}

		return Integer.parseInt(jsoupCommandTowardsFileLinesCount.get(
				jsoupCommandTowardsFileLinesCount.size() - 1).extract(elem));
	}

	/**
	 * Retrieve links ot all files involved in a commit.
	 * @param doc
	 * @return List<Strign> of links
	 */
	private List<String> getHrefToFile(Document doc) {
		
		List<String> hrefs = new ArrayList<String>();
		
		if(doc != null){
			for(Element element : jsoupCommandsTowardsFile.get(0).exec(doc)){
				for(int i = 1; i < jsoupCommandsTowardsFile.size() - 1; i++){
					element = jsoupCommandsTowardsFile.get(i).exec(element);
				}
				
				hrefs.add(jsoupCommandsTowardsFile
					.get(jsoupCommandsTowardsFile.size() - 1).extract(element));
				
			}
		}

		return hrefs;
	}
	
	/**
	 * 
	 * @param AbstractJsoupCommand
	 */
	public void addCommandTowardsFile(AbstractJsoupCommand AbstractJsoupCommand) {
		this.jsoupCommandsTowardsFile.add(AbstractJsoupCommand);
	}

	
	/**
	 * 
	 * @param AbstractJsoupCommand
	 */
	public void addCommandTowardsFileLinesCount(
			AbstractJsoupCommand AbstractJsoupCommand) {
		this.jsoupCommandsTowardsFile.add(AbstractJsoupCommand);
	}
	
	/**
	 * 
	 * @param jsoupCommandsTowardsFile
	 */

	public void setJsoupCommandsTowardsFile(
			List<AbstractJsoupCommand> jsoupCommandsTowardsFile) {
		this.jsoupCommandsTowardsFile = jsoupCommandsTowardsFile;
	}

	
	/**
	 * 
	 * @param jsoupCommandTowardsFileLinesCount
	 */
	public void setJsoupCommandTowardsFileLinesCount(
			List<AbstractJsoupCommand> jsoupCommandTowardsFileLinesCount) {
		this.jsoupCommandTowardsFileLinesCount = jsoupCommandTowardsFileLinesCount;
	}

	
	/**
	 * Responsible to check the current line of the changeset 
	 * against the keywords of the specialized language. 
	 * 
	 * @param line
	 * @return
	 */
	protected abstract boolean checkAgainstKeyWords(String line);

	/**
	 * 
	 * @return
	 */
	public List<String> getAddedStructuralChanges() {
		return addedStructuralChanges;
	}

	
	/**
	 * 
	 * @return
	 */
	public List<String> getDeletedStructualChanges() {
		return deletedStructualChanges;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public List<String> getFiles() {
		return files;
	}

	
	/**
	 * 
	 * @return
	 */
	public Double getPositiveMutation() {
		return positiveMutation;
	}

	
	/**
	 * 
	 * @return
	 */
	public Double getNegativeMutation() {
		return negativeMutation;
	}

	
	/**
	 * 
	 * @return
	 */
	public int getNbLines() {
		return nbLines;
	}

}
