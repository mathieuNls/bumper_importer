package com.concordia.bumper.attachment;

import java.net.MalformedURLException;
import java.net.URL;

import com.concordia.bumper.enums.AttachmentType;

/**
 * This is the mother class of all attachment. Attachment can be text files or changsets.
 * Attachment will be downloaded, understand the corresponding url accescced and fetched
 * when the document they belong to is finish writing. 
 *  
 * @author math
 *
 */
public abstract class AbstractAttachment {
	
	protected URL url;
	protected AttachmentType type;
	protected String output = "";
	
	/**
	 * Simple constructor
	 * @param url
	 * @param type
	 */
	public AbstractAttachment(String url, AttachmentType type){
		try {
			this.url = new URL(url);
			this.type = type;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Specify how a said attachment should be 
	 * downloaded
	 */
	public abstract void download();

	/**
	 * Retrun the type of an attachment.
	 * @see AttachmentType
	 * @return
	 */
	public AttachmentType getType() {
		return type;
	}

	/**
	 * @return the output of attahment. That can be text or a link to
	 * the attachment in case of image for example.
	 */
	public String getOutput() {
		return output;
	}
}
