package com.concordia.bumper.launcher;

import java.io.IOException;

import com.concordia.bumper.enums.JSoupCommands;
import com.concordia.bumper.enums.PatternURL;
import com.concordia.bumper.parser.AbstractParser;
import com.concordia.bumper.parser.BugzillaParser;
import com.concordia.bumper.parser.JiraParser;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
//		AbstractParser bugzillaParser = new 
//				BugzillaParser("data/netbeans.xml",
//				"data/netbeans_solr.xml", 
//				"netbeans", 
//				"https://netbeans.org/bugzilla/attachment.cgi?id=",
//				PatternURL.Netbeans,
//				JSoupCommands.Netbeans);
//		
//		bugzillaParser.parse();
		
		AbstractParser jiraParser = new JiraParser("data/apache/apachejira.xml",
				"data/apache/apache_solr.xml", 
				"apache", 
				"https://issues.apache.org/jira/");
		
		jiraParser.parse();
		
	}

}
