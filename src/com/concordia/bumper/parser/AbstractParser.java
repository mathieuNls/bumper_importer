package com.concordia.bumper.parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.concordia.bumper.attachment.AbstractAttachment;
import com.concordia.bumper.attachment.changeset.AbstractChangesetAttachment;
import com.concordia.bumper.enums.AttachmentType;
import com.concordia.bumper.enums.DocumentType;

/**
 * Abstract parser defining the process of writing / reading as 
 * as same as the replace xml helper function. The replace functions
 * aim to transform raw files to solr compliant files.
 * 
 * I deal w/ xml file as they were plain text for performances. I use readLine 
 * instead of DOM document that are loaded in full on memory and also because
 * xml file I deal with might not be valid.
 * 
 * @author math
 *
 */
public abstract class AbstractParser {
	
	protected String repoName = "default";
	protected String downloadURL = "defaut";
	protected BufferedReader br;
	protected BufferedWriter bw;
	protected List<AbstractAttachment> attachments = new ArrayList<AbstractAttachment>();
	protected int nbBugs = 0;
	protected String outputFileName;
	protected String key;
	
	/**
	 * Fully qualified names are required here
	 * @param inputFileName
	 * @param outputFileName
	 */
	public AbstractParser(String inputFileName, String outputFileName){
		try {
			this.br = new BufferedReader(new FileReader(new File(inputFileName)));
			this.bw = new BufferedWriter(new FileWriter(new File(outputFileName)));
			this.outputFileName = outputFileName;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Main loop that write / read and pass the control the 
	 * effective implementation of Parser to remove / add 
	 * the correct lines.
	 * 
	 * @throws IOException
	 */
	public void parse() throws IOException{
		String line = "";
		Integer nbLine = 0;
		
		bw.write("<add>");
		while((line = this.br.readLine()) != null){
		
			
			
			String transformed = this.transform(line);
			
			bw.write(transformed);
			nbLine++;
			
			if(nbLine % 1000 == 0){
				System.out.println("Reach line: " + nbLine + " on bug " + nbBugs);
				this.bw.flush();
			}
		}
		bw.write("</add>");
		this.br.close();
		this.bw.flush();
		this.bw.close();
	}
	
	/**
	 * Cut the files into n files of length x bugs.
	 * @param lastLine
	 * @throws IOException
	 */
	protected void save(String lastLine) throws IOException{
		bw.write(lastLine);
		bw.write("</add>");
		this.bw.flush();
		this.bw.close();
		this.bw = new BufferedWriter(new FileWriter(new File(this.outputFileName.replace(".xml", "_")+(this.nbBugs / 100)+".xml")));
		bw.write("<add>");
	}
	
	/**
	 * Write all the attachments to the document.
	 * Attachments can be simple attached text files or changeset
	 * Changeset are child of AbstractChangesetAttachment while text file are from FileAttachment
	 * 
	 * Also, changesets are containing a lot of extracted statistics 
	 * 
	 * @see AbstractChangesetAttachment
	 * @see AbstractAttachment
	 * @see FileAttachment
	 * @throws IOException
	 */
	protected void writeAttachments() throws IOException{
		
		//Statistics for the document
		Integer nbChangeSet = 0;
		Integer nbChangesFiles = 0;
		Integer nbAddedStructuralChange = 0;
		Integer nbDeletedStructuralChange = 0;
		Double positiveMutation = 0.0D;
		Double negativeMutation = 0.0D;
		Integer nbLines = 0;
		
		System.out.println("Bug :" + nbBugs + " [" + this.key + "] has " + this.attachments.size() + " attachment(s)");
		
		// Adding attachment fold into the child responsibility
		for(AbstractAttachment attachment : this.attachments){
			
			//Kicks in the download / analysis of attachment
			attachment.download();
			
			this.writeField(attachment.getOutput(), getFieldNameNyType(attachment.getType()));
			
			//In case we deal w/ a changeset, pull up statistics and write multiValued fields
			if(attachment.getType().equals(AttachmentType.CHANGESET)){
				
				System.out.println("Bug :" + nbBugs + " has changeset");
				
				AbstractChangesetAttachment changeset = (AbstractChangesetAttachment) attachment;
				nbAddedStructuralChange += changeset.getAddedStructuralChanges().size();
				nbDeletedStructuralChange += changeset.getDeletedStructualChanges().size();
				nbChangesFiles += changeset.getFiles().size();
				
				this.writeListofField(changeset.getAddedStructuralChanges(), "added_structual_change");
				this.writeListofField(changeset.getDeletedStructualChanges(), "deleted_structual_change");
				this.writeListofField(changeset.getFiles(), "modificated_file");
				
				positiveMutation += changeset.getPositiveMutation();
				negativeMutation += changeset.getNegativeMutation();
				
				nbChangeSet++;
				nbLines += changeset.getNbLines();
				
			}
		}
		
		// Write statistics
		this.writeField(nbAddedStructuralChange.toString(), "added_structual_changes_number");
		this.writeField(nbDeletedStructuralChange.toString(), "deleted_structual_changes_number");
		this.writeField(nbChangeSet.toString(), "change_set_number");
		this.writeField(nbChangesFiles.toString(), "modificated_files_number");
		this.writeField(nbLines.toString(), "lines_in_modificated_files_number");
		
		if(nbChangeSet > 1){
			positiveMutation = positiveMutation / nbChangeSet;
			negativeMutation = negativeMutation / nbChangeSet;
		}
		
		positiveMutation = Math.round(positiveMutation * 100.0) / 100.0;
		negativeMutation = Math.round(negativeMutation * 100.0) / 100.0;
		
		this.writeField(positiveMutation.toString(), "positive_mutation");
		this.writeField(negativeMutation.toString(), "negative_mutation");
		
		// We move on to the next bug. Remove all existing attachments.
		this.attachments.clear();
	}
	
	/**
	 * Return adequate field name
	 * @param type
	 * @return
	 */
	private String getFieldNameNyType(AttachmentType type){
		if(type.equals(AttachmentType.FILE)){
			return "attachment";
		}else if(type.equals(AttachmentType.CHANGESET)){
			return "change_set";
		}else{
			return "attachment";
		}
	}
	
	
	/**
	 * Will write a list of string.
	 * @see writeField
	 * @param lines
	 * @param tag
	 * @throws IOException
	 */
	protected void writeListofField(List<String> lines, String tag) throws IOException{
		for(String line : lines){
			this.writeField(line, tag);
		}
	}
	
	
	/**
	 * will write <field name="tag">content</field> to the file
	 * 
	 * @param line
	 * @param tag
	 * @throws IOException 
	 */
	protected void writeField(String line, String tag) throws IOException{
		this.bw.write("<field name='"+tag+"'>"+escapeXML(line)+"</field>");
	}
	
	
	/**
	 * Transform <tag>164543</tag>
	 * to <field name='tag'>prepend_164543</field>
	 * 
	 * @param line
	 * @param tag
	 * @param prepend
	 * @return
	 */
	protected String conserveXMLTagNameAndPrependData(String line, String tag, String prepend){
		line = line.split(">")[1].replaceAll("</"+tag, "");
		return "<field name='"+tag+"'>"+prepend+"_"+escapeXML(line)+"</field>";
	}
	
	
	/**
	 * Transform <tag>164543</tag>
	 * to <field name='tag'>164543</field>
	 * @param line
	 * @param tag
	 * @return
	 */
	protected String conserveXMLTagName(String line, String tag){
		line = line.split(">")[1].replaceAll("</"+tag, "");
		return "<field name='"+tag+"'>"+escapeXML(line)+"</field>";
	}
	
	
	/**
	 * Transform <oldTag>164543</oldTag>
	 * to <field name='newTag'>prepend_164543</field>
	 * 
	 * @param line
	 * @param oldTag
	 * @param newTag
	 * @param prepend
	 * @return
	 */
	protected String replaceXMLTagNameAndPrependData(String line, String oldTag, String newTag, String prepend){
		line = line.replaceAll(oldTag, newTag);
		return conserveXMLTagNameAndPrependData(line, newTag, prepend);
	}
	
	
	/**
	 * Transform <oldTag>164543</oldTag>
	 * to <field name='newTag'>164543</field>
	 * 
	 * @param line
	 * @param oldTag
	 * @param newTag
	 * @return
	 */
	protected String replaceXMLTag(String line, String oldTag, String newTag){
		line = line.replaceAll(oldTag, newTag);
		return this.conserveXMLTagName(line, newTag);
	}
	
	
	/**
	 * Transform <tag attribute="_ tboudreau">tboudreau</tagName>
	 * to <field name='tag'>_ tboudreau</field>
	 * @param line
	 * @param tag
	 * @param attribute
	 * @return
	 */
	protected String conserveXMLTagNameAndUseAttribute(String line, String tag, String attribute){
		String[] split = line.split(attribute+"=\"");
		String attributValue = split[1].split("\"")[0];
		return "<field name='"+tag+"'>"+attributValue+"</field>";
	}
	

	/**
	 * 
	 * Transform <whatever attribute="_ tboudreau">tboudreau</whatever>
	 * to <field name='newTag'>_ tboudreau</field>
	 * 
	 * @param line
	 * @param newTag
	 * @param attribute
	 * @return
	 */
	protected String replaceXMLTagNameAndUseAttribute(String line, String newTag, String attribute){
		String[] split = line.split(attribute+"=\"");
		//Attribute doesn't exist
		if(split.length == 1){
			return "";
		}else{
			String attributValue = split[1].split("\"")[0];
			return "<field name='"+newTag+"'>"+attributValue+"</field>";
		}
	}
	
	
	/**
	 * Transform     <tag>Hmm, what do you expect? 
	 *	
	 *	ie.
	 *	TypeVariable&lt;? extends Class&lt;? extends Map&gt;&gt;[] x = data.getClass().getTypeParameters();
	 *	
	 *	? Thanks.</tag>
	 *
	 *in
	 *
	 * <field name='newTag'>Hmm, what do you expect?\nie.\nTypeVariable&lt;? ex...</field>
	 *
	 * Note the blank line replaced by \\n to reduce the output file lines. 
	 * The displaying program will have to transform \n the adapted carriage return symbol
	 * 
	 * Also, space are trimmed for the very same reason. That could be damaging, however, 
	 * where spaces are used to enhance the comprehension as in code sample.
	 *
	 * @param line
	 * @param tag
	 * @param newTag
	 * @return
	 * @throws IOException 
	 */
	protected String replaceMultiLineXMLTagName(String line, String tag, String newTag) throws IOException{
		
		return "<field name='"+newTag+"'>"+this.extractDataMultiline(line, tag)+"</field>";
	}
	

	/**
	 * Will we store comment, bug and changeset for sure
	 * and maybe other type of documents. This is an helper
	 * function to append the type to a document
	 * 
	 * @param type
	 * @return
	 */
	protected String appendType(DocumentType type){
		return "<field name='type'>"+type+"</field>";
	}
	

	/**

	 * Transform <field name='whatever'>NotWellFormatedDate</field>
	 * to <field name='whatever'>WellFormatedDate</field>
	 * 
	 * @see http://lucene.apache.org/solr/4_4_0/solr-core/org/apache/solr/schema/DateField.html
	 * @param line
	 * @return
	 */
	protected String convertDateField(String line){
		String tmp = line.split(">")[1].split("<")[0];
		tmp = this.convertDate(tmp);
		return line.split(">")[0] + ">" + escapeXML(tmp) + "</field>"; 
	}
	

	/**

	 * Transform <wathevertage>wahteverdata</wathevertage>
	 * to wahteverdata
	 * 
	 * @param line
	 * @param tag
	 * @return
	 */
	protected String extractData(String line, String tag){
		return escapeXML(line.split(">")[1].replace("</"+tag, ""));
	}
	

	/**

	 * <wathevertage attribute="whateverattributedate">wahteverdata</wathevertage>
	 * to whateverattributedate
	 * 
	 * @param line
	 * @param attribute
	 * @return
	 */
	protected String extractAttribute(String line, String attribute){
		return line.split(attribute+"=\"")[1].split("\"")[0];
	}
	
	
	/**
	 * Transform     <tag>Hmm, what do you expect? 
	 *	
	 *	ie.
	 *	TypeVariable&lt;? extends Class&lt;? extends Map&gt;&gt;[] x = data.getClass().getTypeParameters();
	 *	
	 *	? Thanks.</tag>
	 *
	 *in
	 *
	 * >Hmm, what do you expect?\nie.\nTypeVariable&lt;? ex...
	 *
	 * Note the blank line replaced by \\n to reduce the output file lines. 
	 * The displaying program will have to transform \n the adapted carriage return symbol
	 * 
	 * Also, space are trimmed for the very same reason. That could be damaging, however, 
	 * where spaces are used to enhance the comprehension as in code sample.
	 * @param line
	 * @param tag
	 * @return
	 * @throws IOException
	 */
	protected String extractDataMultiline(String line, String tag) throws IOException{
		
		boolean wasOneLine = true;
		
		String[] splittedString = line.split("<"+tag+">");
		
		String tmp = "";
		
		if(splittedString.length == 2){ // In case the "<"+tag+">" isn't alone on the line
			tmp = line.split("<"+tag+">")[1].trim() + "\n";
			
		}
		
		while(!line.contains("</"+tag+">") && !(line = this.br.readLine()).contains("</"+tag+">")){
			wasOneLine = false;
			if(line.isEmpty()){
				tmp = tmp + "\n";
			}else{
				tmp = tmp + line.trim() + "\n";
			}
		}
		
		//Even thought this method is made for multi lines xml content,
		//We can't know in advance if this multi lines field contains only
		//one line. Hence, the specific behavior to get the last line here.
		if(!wasOneLine){
			tmp = tmp + line.trim();
		}
		
		tmp = tmp.replace("</"+tag+">", "");
		return escapeXML(tmp);
	}
	
	
	/**
	 * Append a field
	 * @param name
	 * @param data
	 * @return
	 */
	protected String appendField(String name, String data){
		return "<field name='"+name+"'>"+escapeXML(data)+"</field>"; 
	}
	

	/**
	 * Child have to define what are the tags to look at / replace.
	 * @param line
	 * @return
	 */
	protected abstract String transform(String line) throws IOException;

	
	/**
	 * Child must provide their own date converter
	 * 
	 * @param date
	 * @return
	 */
	protected abstract String convertDate(String date);	
	
	/**
	 * Attempt to replace all char that will produce invalid xml file
	 * @param data
	 * @return escapedXML
	 */
	private String escapeXML(String data){
		data = data.replaceAll("&&", "&amp;&amp;");
		return data.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

}
