package com.concordia.bumper.parser;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.concordia.bumper.attachment.FileAttachment;
import com.concordia.bumper.attachment.changeset.AbstractChangesetAttachment;
import com.concordia.bumper.attachment.changeset.JavaChangesetAttachment;
import com.concordia.bumper.enums.AttachmentType;
import com.concordia.bumper.enums.DocumentType;
import com.concordia.bumper.enums.JSoupCommands;
import com.concordia.bumper.enums.PatternURL;
import com.concordia.bumper.enums.Seperator;
import com.concordia.bumper.helpers.UrlHelper;


/**
 * Specialized parser for Bugzilla xml files
 * 
 * @author math
 *
 */
public class BugzillaParser extends AbstractParser{

	private PatternURL patternURL;
	private JSoupCommands jsoupCommands;
	private String completeProject = "";
	private Integer nbComments = 0;
	private String lastDate;
	private String submittedDate;
	
	/**
	 * Nothing more to do than call super in here.
	 * 
	 * @param inputFileName
	 * @param outputFileName
	 */
	public BugzillaParser(String inputFileName, String outputFileName, String repoName, String downloadURL,
			PatternURL patternURL, JSoupCommands jsoupCommands) {
		super(inputFileName, outputFileName);
		this.repoName = repoName;
		this.downloadURL = downloadURL;
		this.patternURL = patternURL;
		this.jsoupCommands = jsoupCommands;
	}
	
	@Override
	public String transform(String line) throws IOException {
		if(line.contains("<bug>")){
			return "<doc>";
			
		}else if(line.contains("<bug_id>")){

//			<bug_id>164543</bug_id>
			return this.replaceXMLTagNameAndPrependData(line, "bug_id", "id", "bug_"+this.repoName);
		}else if(line.contains("<creation_ts>")){
			this.submittedDate = this.extractData(line, "creation_ts");
			return this.convertDateField(this.replaceXMLTag(line, "creation_ts", "date"));
		}else if(line.contains("<short_desc>")){
			
//		    <short_desc>Change type hint generates bizarre code</short_desc>
			return this.replaceXMLTag(line, "short_desc", "title");
			
		}else if(line.contains("<product>")){
//		    <product>java</product>
			this.completeProject += this.extractData(line, "product");
			return this.replaceXMLTag(line, "product", "project");
			
		}else if(line.contains("<component>")){
//		    <component>Hints</component>
			this.completeProject += "-" + this.extractData(line, "component");
			return this.replaceXMLTag(line, "component", "sub_project");
			
		}else if(line.contains("<version>")){
//		    <version>6.x</version>
			return this.conserveXMLTagName(line, "version");
			
		}else if(line.contains("<rep_platform>")){
//		    <rep_platform>All</rep_platform>
			return this.replaceXMLTag(line, "rep_platform", "impacted_plateform");
			
		}else if(line.contains("<op_sys>")){
//		    <op_sys>All</op_sys>
			return this.replaceXMLTag(line, "op_sys", "impacted_os");
			
		}else if(line.contains("<bug_status>")){
//		    <bug_status>RESOLVED</bug_status>
			return this.conserveXMLTagName(line, "bug_status");
			
		}else if(line.contains("<resolution>")){

//		    <resolution>FIXED</resolution>
			return this.conserveXMLTagName(line, "resolution");
			
		}else if(line.contains("<bug_file_loc>")){
//			<bug_file_loc></bug_file_loc>
			return this.replaceXMLTag(line, "bug_file_loc", "presume_bug_loc");
			
		}else if(line.contains("<bug_severity>")){
//			<bug_severity>blocker</bug_severity>
			return this.conserveXMLTagName(line, "bug_severity");
			
		}else if(line.contains("<target_milestone>")){
//			<target_milestone>6.x</target_milestone>
			return this.conserveXMLTagName(line, "target_milestone");
			
		}else if(line.contains("<reporter") && !line.contains("_accessible")){
//	          <reporter name="_ tboudreau">tboudreau</reporter>
			return this.replaceXMLTag(line, "reporter", "reporter_pseudo")
					+ this.replaceXMLTagNameAndUseAttribute(line, "reporter_name", "name");
			
		}else if(line.contains("<assigned_to name=")){
//	          <assigned_to name="Jan Lahoda">jlahoda</assigned_to>
			return this.replaceXMLTag(line, "assigned_to", "assigned_to_pseudo")
					+ this.replaceXMLTagNameAndUseAttribute(line, "assigned_to_name", "name");
		}else if(line.contains("<commentid>")){
			
			this.nbComments++;

//		<long_desc isprivate="0" >
//		    <commentid>85133</commentid>
//		    <comment_count>1</comment_count>
//		    <who name="Jiri Prox">jiriprox</who>
//		    <bug_when>2009-05-06 10:18:45 +0000</bug_when>
//		    <thetext>reproducible</thetext>
//		  </long_desc>
			
			line = this.br.readLine();
			
			// In bugzilla, the first comment is actually the bug description...
			if(line.contains("<comment_count>0</comment_count>")){
				this.br.readLine(); //we are on who tag
				this.br.readLine(); //we are on the bug_when tag
				line = this.br.readLine(); //we are on the thetext tag
				
				return this.replaceMultiLineXMLTagName(line, "thetext", "description");
			
			}else{ 
				//And other comments are classical comments
				
				String comment = "";
				String attachid = "";
				line = this.br.readLine();
			
				// In case the comment was generated because of an attachment.
				if(line.contains("<attachid>")){
					attachid = this.extractData(line, "attachid");
					this.attachments.add(
							new FileAttachment(this.downloadURL+attachid,  AttachmentType.FILE)
					);
					line = this.br.readLine();
				}
				
				comment += this.extractData(line, "who") + Seperator.COMMENT.getValue();
				comment += this.extractAttribute(line, "name") + Seperator.COMMENT.getValue();
				
				line = this.br.readLine();
				comment += this.convertDate(this.extractData(line, "bug_when")) + Seperator.COMMENT.getValue();
				
				line = this.br.readLine();
				String text = this.extractDataMultiline(line, "thetext");
				
				List<String> links = UrlHelper.extractFromTextWithPattern(text, this.patternURL.getPattern());
				
				for(String link : links){
					AbstractChangesetAttachment cs = new JavaChangesetAttachment(link, AttachmentType.CHANGESET, "pre");
					cs.setJsoupCommandsTowardsFile(this.jsoupCommands.forFile());
					cs.setJsoupCommandTowardsFileLinesCount(this.jsoupCommands.ForFileLine());
					this.attachments.add(cs);
				}
				
				comment += text + Seperator.COMMENT.getValue() + attachid;
				
				return appendField("comment", comment);
			}
			
		}else if(line.contains("</bug>")){
			
			this.nbBugs++;

				this.wrapUp();
				this.writeAttachments();
				
				if(this.nbBugs % 100 == 0){
					this.save(this.appendType(DocumentType.BUG) + "</doc>");
				}else{
					return this.appendType(DocumentType.BUG) + "</doc>";
				}
		}

		return "";
	}
	
	/**
	 * Write computed fields
	 * @throws IOException
	 */
	private void  wrapUp() throws IOException{
		 @SuppressWarnings("deprecation")
		 long diffInMillies =  new Date(this.lastDate).getTime() - new Date(this.submittedDate).getDate(); 
		 Long days = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS);
		 
		 this.nbComments = 0;
		 this.lastDate = "";
		 this.submittedDate = "";
		 
		 this.writeField(days.toString(), "ellapsed_days");
		 this.writeField(this.nbComments.toString(), "comments_nb");
		 this.writeField(this.completeProject, "full_project_name");
	}

	@Override
	protected String convertDate(String date) {
		// 2009-05-13 07:40:04 +0000 
		// to 1995-12-31T23:59:59.999Z
		
		this.lastDate = date;
		
		date = date.replaceFirst(" ", "T");
		date = date.replaceFirst(" ", "Z");
		date = date.replace("+0000", "");
		
		return date;
	}
	

}
