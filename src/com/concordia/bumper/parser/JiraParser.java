/**
    Copyright (C) 2014 Mathieu Nayrolles

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.concordia.bumper.parser;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.concordia.bumper.attachment.changeset.AbstractChangesetAttachment;
import com.concordia.bumper.attachment.changeset.JavaChangesetAttachment;
import com.concordia.bumper.enums.AttachmentType;
import com.concordia.bumper.enums.DocumentType;
import com.concordia.bumper.enums.Seperator;

public class JiraParser extends AbstractParser{

	private String lastDate;
	private String submittedDate;
	private Integer nbComments = 0;
	private String attachmentDownloadBase;
	private Integer reop = 0;
	
	public JiraParser(String inputFileName, String outputFileName,String repoName, String attachmentDownloadBase) {
		super(inputFileName, outputFileName);
		this.repoName = repoName;
		this.attachmentDownloadBase = attachmentDownloadBase;
//				https://issues.apache.org/jira/
	}
	
	

	@Override
	protected String transform(String line) throws IOException {

		
		if(line.contains("<item>")){
			
			return "<doc>";
			
		}else if(line.contains("<title>")){
			
//			<title>[SPARK-515] Make it easier to run external code on a Nexus cluster</title>
			return this.conserveXMLTagName(line, "title");
			
		}else if(line.contains("<project>")){
			
//			 <project id="12315420" key="SPARK">Spark</project>
			return this.conserveXMLTagName(line, "project");
			
		}else if(line.contains("<reporter username")){
//			<reporter username="yuzhihong@gmail.com">Ted Yu</reporter>
			
			return this.replaceXMLTag(line, "reporter", "reporter_pseudo")
					+ this.replaceXMLTagNameAndUseAttribute(line, "reporter_name", "username");
			
		}else if(line.contains("<assignee username")){
			
//			<assignee username="zhihyu@ebaysf.com">Ted Yu</assignee>
			return this.replaceXMLTag(line, "assignee", "assigned_to_pseudo")
					+ this.replaceXMLTagNameAndUseAttribute(line, "assigned_to_name", "username");
			
		}else if(line.contains("<description>")){
		
//			<description>
			return this.replaceMultiLineXMLTagName(line, "description", "description");
			
		}else if(line.contains("<key")){
			
			this.key = this.extractData(line, "key");
			return this.replaceXMLTagNameAndPrependData(line, "key", "id", "bug_"+this.repoName);
			
		}else if(line.contains("<created>")){
			
			this.submittedDate = this.convertDate(this.extractData(line, "created"));
			return this.appendField("created", this.submittedDate);
			
		}else if(line.contains("<resolution")){
			
//			<resolution id="1">Fixed</resolution>
			return this.replaceXMLTag(line, "resolution", "resolution");
			
		}else if(line.contains("<component>")){
//			<component>tests</component>
			return this.replaceXMLTag(line, "component", "sub_project");
		
		}else if(line.contains("<version>")){
			
//			<version>3.5.0</version>
			
			return this.conserveXMLTagName(line, "version");
			
		}else if(line.contains("<resolved>")){
		
//			<resolved>Fri, 19 Oct 2012 23:50:39 +0100</resolved>
			this.lastDate = this.convertDate(this.extractData(line, "resolved"));
			
			@SuppressWarnings("deprecation")
			long diffInMillies =  new Date(this.lastDate).getTime() - new Date(this.submittedDate).getTime(); 
			Long days = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS); 
			
			this.writeField(days.toString(), "ellapsed_days");
			
		}else if(line.contains("<priority id=")){
			
//			<priority id="2" iconUrl="https://issues.apache.org/jira/images/icons/priorities/critical.png">Critical</priority>
			
			return this.replaceXMLTag(line, "priority", "bug_severity");
			
		}else if(line.contains("<comment id=")){
			
			nbComments++;
			
//			<comment id="13952612" author="patrick" created="Mon, 11 Oct 2010 20:46:00 +0100"  >&lt;p&gt;Github comment 
			
			String comment =  this.extractAttribute(line, "author") + Seperator.COMMENT.getValue();
			comment +=  this.convertDate(this.extractAttribute(line, "created")) + Seperator.COMMENT.getValue();
			comment += this.extractDataMultiline(line, "comment");
			return appendField("comment", comment);
			
		}else if(line.contains("<attachment id=") && 
				this.extractAttribute(line, "name").contains(".patch")){
			
			String idAttachment = this.extractAttribute(line, "id");
			System.out.println("Adding attachment for bug " + this.key);
			AbstractChangesetAttachment cs = new JavaChangesetAttachment(this.attachmentDownloadBase+"secure/attachment/"+idAttachment+"/", 
					AttachmentType.CHANGESET, "body", true);
			this.attachments.add(cs);
			
		}else if(line.contains("</item>")){
			this.nbBugs++;
			this.wrapUp();
			
			if(this.nbBugs % 100 == 0){
				this.save(this.appendType(DocumentType.BUG) + "</doc>");
			}else{
				return this.appendType(DocumentType.BUG) + "</doc>";
			}
		}
		
		return "";
	}
	
	private void  wrapUp() throws IOException{
		this.writeField(this.nbComments.toString(), "comments_nb");
		this.writeField("RESOLVED", "bug_status");
		Document doc = null;
		try {
			doc = Jsoup.connect(this.attachmentDownloadBase + "browse/" + this.key).timeout(10*1000).get();
			for (Element element : doc.getElementsByClass("issue-data-block")) {
				for(Element subElement : element.getElementsByClass("activity-new-val")){
					if(subElement.text().trim().contains("Reopened")){
						this.reop++;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error Jsouping: " + this.key);
		}
		
		this.writeField(this.reop.toString(), "reopening");
		
		this.reop = 0;
		 this.nbComments = 0;
		 this.lastDate = "";
		 this.submittedDate = "";
	
		this.writeAttachments();
	}
	

	@Override
	protected String convertDate(String date) {
		//Thu, 1 Apr 0010 23:02:45 -0001
		date = date.replace(" 00", " 20");
		date = date.split(", ")[1];
		if(date.contains("-")){
			date = date.split(" -")[0];
		}else if(date.contains("+")){
			date = date.split(" \\+")[0];
		}
		
		return date;
	}
	
	

}
