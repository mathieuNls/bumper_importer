package com.concordia.bumper.enums;

/**
 * Defines the pattern URL to look for in comments.
 * 
 * @author math
 *
 */
public enum PatternURL {

	Netbeans("(http:\\/\\/)?(hg.netbeans.org\\/)(.*\\/)(\\/rev)?([\\/\\w \\.-]*)");
	
	private String pattern;
	
	private PatternURL(String pattern){
		this.pattern = pattern;
	}
	
	public String getPattern(){
		return this.pattern;
	}
}
