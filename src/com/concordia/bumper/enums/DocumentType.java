package com.concordia.bumper.enums;

/**
 * Type of accepted documents
 * 
 * @todo the only used type is bug. We maybe can get rid 
 * of that.
 * 
 * @author math
 *
 */
public enum DocumentType {
	COMMENT, BUG, CHANGESET;
}
