package com.concordia.bumper.enums;

/**
 * Defines the separator to be used between comments field.
 * 
 * @author math
 *
 */
public enum Seperator {
	COMMENT("*`|`*");
	
	private String value ="default";
	
	private Seperator(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
