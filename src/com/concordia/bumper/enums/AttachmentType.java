package com.concordia.bumper.enums;

/**
 * Enum for the attachment possible type.
 * @author math
 *
 */
public enum AttachmentType {
	FILE, CHANGESET;
}
