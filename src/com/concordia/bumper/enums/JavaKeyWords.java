package com.concordia.bumper.enums;

/**
 * Java usefull keyworkds
 * 
 * @author math
 *
 */
public enum JavaKeyWords {
	ABSTRACT, BREAK, CASE, CATCH, CLASS, CONTINUE, DEFAULT, DO, ELSE, ENUM, FINAL, 
	FINALLY, FOR, IF,  IMPLEMENTS, RETURN, SWITCH, SYNCHRONIZED, THROW, THROWS, TRY,
	VOLATILE, WHILE, NULL;
}
