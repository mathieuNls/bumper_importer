package com.concordia.bumper.enums;

import java.util.ArrayList;
import java.util.List;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.concordia.bumper.attachment.changeset.AbstractJsoupCommand;

/**
 * Implementation of the @see http://www.oodesign.com/adapter-pattern.html and 
 * @see http://www.oodesign.com/command-pattern.html. Each enum will defines a 
 * specific set of JsoupCommand for a bug provider and adapt the command provided 
 * by @see AbstractJSoupCommands
 * 
 * @author math
 *
 */
public enum JSoupCommands {
	
	@SuppressWarnings("serial")
	Netbeans(new ArrayList<AbstractJsoupCommand>(){{
			  add(new AbstractJsoupCommand(){ public Elements exec(Document doc){ return doc.getElementsByClass("link"); } });
			  add(new AbstractJsoupCommand(){ public Element exec(Element elem){ return elem.getElementsByTag("a").get(0); } });
			  add(new AbstractJsoupCommand(){ public String extract(Element elem){ return "http://hg.netbeans.org/" + elem.attr("href") ; } });
			}}, 
			new ArrayList<AbstractJsoupCommand>(){{
				add(new AbstractJsoupCommand(){ public Elements exec(Document doc){ return doc.getElementsByClass("page_body") ; } });
				add(new AbstractJsoupCommand(){ public String extract(Element elem){ return ""+elem.getElementsByClass("linenr").size() ; } });
			}});
	
	private List<AbstractJsoupCommand> jsoupCommandsToFile;
	private List<AbstractJsoupCommand> jsoupCommandsToFileLine;
	
	private JSoupCommands(List<AbstractJsoupCommand> jsoupCommandsToFile, 
			List<AbstractJsoupCommand> jsoupCommandsToFileLine){
		this.jsoupCommandsToFile = jsoupCommandsToFile;
		this.jsoupCommandsToFileLine = jsoupCommandsToFileLine;
	}

	public List<AbstractJsoupCommand> forFile() {
		return jsoupCommandsToFile;
	}

	public List<AbstractJsoupCommand> ForFileLine() {
		return jsoupCommandsToFileLine;
	}
}
