package com.concordia.bumper.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple helper to extract all the url out of a bunch of text.
 * @author math
 *
 */
public class UrlHelper {
	
	/**
	 * Extract all url no matter what
	 * @param text
	 * @return
	 */
	public static List<String> extractFromText(String text){
		
		String regex = "^\u00A3(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?\u00A3$";
		
		return extractFromTextWithPattern(text, regex);
		
	}
	
	/**
	 * Extract url according to a specific pattern. Likely provided by @see PatternURL
	 * @param text
	 * @param pattern
	 * @return
	 */
	public static List<String> extractFromTextWithPattern(String text, String pattern){
		List<String> links = new ArrayList<String>();
	
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(text);
		while(m.find()) {
			String urlStr = m.group();
				if (urlStr.startsWith("(") && urlStr.endsWith(")"))
				{
					urlStr = urlStr.substring(1, urlStr.length() - 1);
				}
				if(!urlStr.contains("http://")){
					urlStr = "http://" + urlStr;
				}
				if(urlStr.endsWith(".")){
					urlStr = urlStr.substring(0, urlStr.lastIndexOf("."));
				}
			links.add(urlStr);
		}
		return links;
	}

}
