## What is it ?

Bumber backend that parses downloaded bugs using the technique depicted here <video link tba> and produce Solr compatible XML files.

## Supported Repo and owned bug (9. oct)

* Bugzilla
* * Netbeans 60K
* * Mozilla 10K *TO TEST*
* * Eclispe 90K *TO TEST*
* * Apache 20K *TO TEST*
* SourceForge *TO DO*
* * dnsjava 19
* * jfreechart 559
* JIRA *TO DO*
* * Apache 58K
* * pdfbox 101